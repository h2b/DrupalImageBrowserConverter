/*
  Drupal ImageBrowser Converter

  Copyright 2016 Hans-Hermann Bode

  Licensed under the EUPL V.1.1.
*/
package models

import org.scalatestplus.play._

class DrupalImageTest extends PlaySpec with OneAppPerSuite {

  private val imageService: DrupalImageService = app.injector.instanceOf(classOf[DrupalImageService])

  "imageService" should {

    "retrieve some values" in {
    	val all = imageService.selectAll()
    	assert(all.size>0)
    }

    "retrieve some values with files" in {
    	val all = imageService.selectAllWithFiles()
    	assert(all.size>0)
    }

    "records join correctly" in {
      val all = imageService.selectAllWithFiles()
      for (j ← all) assertResult(j._1.fid)(j._2.fid)
    }

  }

}
