/*
  Drupal ImageBrowser Converter

  Copyright 2016 Hans-Hermann Bode

  Licensed under the EUPL V.1.1.
*/
package models

import org.scalatestplus.play._

class DrupalFilesTest extends PlaySpec with OneAppPerSuite {

  private val service: DrupalFilesService = app.injector.instanceOf(classOf[DrupalFilesService])

  "service" should {

    "be in injected" in {
      assert(service!=null)
    }

    "retrieve some values" in {
    	val all = service.selectAll()
    	assert(all.size>0)
    }

  }

}
