/*
  Drupal ImageBrowser Converter

  Copyright 2016 Hans-Hermann Bode

  Licensed under the EUPL V.1.1.
*/
package utils

import org.scalatestplus.play._

import de.h2b.scala.lib.io._
import models.DrupalImageService
import play.api.Configuration
import play.api.db.DBApi

class ConverterTest extends PlaySpec with OneAppPerSuite {

  private val configuration: Configuration = app.injector.instanceOf(classOf[Configuration])

	implicit private val imageService: DrupalImageService = app.injector.instanceOf(classOf[DrupalImageService])

  private val inputFilename = "input.html"
  private val outputFilename = "output.html"

  "Converter" should {

    "have conversion paths injected" in {
      val path = configuration.underlying.getString("conversion.path.source")
      assert(path.size>0)
    }

    "use a suitable regular expression" in {
    	val regex = configuration.underlying.getString("conversion.path.source").r
    	val probe = "\"/?q=imagebrowser/view/image/94/_original\""
    	var nid = 0
    	var size = ""
    	probe match {
    	  case regex(group1, group2) ⇒ nid = group1.toInt; size = group2
    	}
    	assertResult(94)(nid)
    	assertResult("_original")(size)
    }

    "handle IDs not found in database as expected" in {
    	val regex = configuration.underlying.getString("conversion.path.source").r
			val converter = new Converter(regex, None)
      val probe = "\"/?q=imagebrowser/view/image/999999/_original\""
    	val expected = Converter.failMarker + probe
			val actual = converter.convert(probe)
			assertResult(expected)(actual)
    }

    "convert example input resource to corresponding output resource" in {
    	val regex = configuration.underlying.getString("conversion.path.source").r
			val converter = new Converter(regex, None)
    	val inputSource = resource(getClass, inputFilename)
    	val outputSource = resource(getClass, outputFilename)
    	val input = inputSource.getLines().mkString
    	val output = outputSource.getLines().mkString
    	val converted = converter.convert(input)
    	assertResult(output)(converted)
    	inputSource.close()
    	outputSource.close()
    }

  }

}