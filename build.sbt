name := "Drupal ImageBrowser Converter"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  cache,
  ws,
  jdbc,
  "com.typesafe.play" %% "anorm" % "2.5.2",
  "mysql" % "mysql-connector-java" % "5.1.40",
  "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.1" % Test,
  "de.h2b.scala.lib" %% "utilib" % "0.3.0" % Test
)

fork in run := true
