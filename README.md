# Drupal ImageBrowser Converter

When upgrading from Drupal 6 to Drupal 7, the situation may arise that images are not shown anymore. If you used Imagebrowser to process the images, it uses special links that do not point to the file system directly; since Imagebrowser is not available for Drupal 7, the system cannot handle these links anymore and that might be the cause of the trouble.

This Play-Framework application converts an HTML page containing Imagebrowser links to a page with these links replaced by direct file-system links.

## Prerequisites

* You need everything to compile and run a Scala-Play application, which is basically Java 1.8, SBT and an appropriate database system, e.g., MariaDB or MySQL).

* From your Drupal database, extract the tables `image` and `files` and call them `drupal_image` and `drupal_files`, respectively.

## Configuration

In the `conf` directory, do the following:

* Copy the file `db.conf.template` to `db.conf` and change the settings according to your database.

* Copy the file `secret.conf.template` to `secret.conf` and change the value of the secret. You can use `playGenerateSecret` in SBT to generate a suitable one.

* Look over the file `application.conf` whether the settings are all right. One setting you might want to change is `image.target.size`, if images came in different sizes before (like preview or thumb-nail size in addition to the original size), but not all sizes are present in the file system; if you encounter this problem, try `image.target.size = _original` (see the comments in `application.conf` for more information).

# Usage

* Open an SBT console in the project directory and type `run`. Then open a browser and enter the address `http://localhost:9000`. Make yourself familiar with the (few) links you can click to perform the conversion. Note: If for some reason port `9000` is inappropriate, you can change it using `run 8080` (or another appropriate port number) in SBT.

* If everything works as expected, log into your Drupal installation and open a page you like to convert in an editor. Switch to "source-code mode" (at least CKEditor can do that), copy the whole source code into the input field of this conversion application, click *Convert Input* and replace the original source code by the output you got. Now, check the preview of the new page carefully and -- if everything is okay -- save it.

* Repeat this procedure for the next page you like to convert until you are done.

## Licence

Drupal ImageBrowser Converter

Copyright 2016 Hans-Hermann Bode

Licensed under the EUPL V.1.1.
