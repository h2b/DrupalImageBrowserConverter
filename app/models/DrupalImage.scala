/*
  Drupal ImageBrowser Converter

  Copyright 2016 Hans-Hermann Bode

  Licensed under the EUPL V.1.1.
*/
package models

import anorm._
import anorm.SqlParser._
import javax.inject.Inject
import javax.inject.Singleton
import play.api.db.DBApi

/**
 * Represents a Drupal 6 "image" table as described by
 * {{{
 * +------------+------------------+------+-----+---------+-------+
 * | Field      | Type             | Null | Key | Default | Extra |
 * +------------+------------------+------+-----+---------+-------+
 * | nid        | int(10) unsigned | NO   | PRI | 0       |       |
 * | fid        | int(10) unsigned | NO   | MUL | 0       |       |
 * | image_size | varchar(32)      | NO   | PRI |         |       |
 * +------------+------------------+------+-----+---------+-------+
 * }}}
 *
 * @author h2b
 */
case class DrupalImage (nid: Int, fid: Int, imagesize: String)

@Singleton
class DrupalImageService @Inject() (dbapi: DBApi, dfs: DrupalFilesService) {

  val parser: RowParser[DrupalImage] =
    get[Int]("drupal_image.nid") ~
    get[Int]("drupal_image.fid") ~
    get[String]("drupal_image.image_size") map {
      case nid~fid~imagesize ⇒
        DrupalImage(nid, fid, imagesize)
  }

  val parserWithFiles: RowParser[(DrupalImage, DrupalFiles)] = parser ~ dfs.parser map {
    case image~file ⇒ (image, file)
  }

  private val db = dbapi.database("default")

  private val queryAll = SQL("select * from drupal_image")
  private val queryAllWithFiles = SQL(
      """
        select * from drupal_image
        join drupal_files on drupal_image.fid = drupal_files.fid
      """)

  def selectAll (): List[DrupalImage] = db.withConnection { implicit connection ⇒
	  queryAll.as(parser.*)
  }

  def selectAllWithFiles (): List[(DrupalImage, DrupalFiles)] = db.withConnection { implicit connection =>
    queryAllWithFiles.as(parserWithFiles.*)
  }

}
