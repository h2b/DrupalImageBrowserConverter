/*
  Drupal ImageBrowser Converter

  Copyright 2016 Hans-Hermann Bode

  Licensed under the EUPL V.1.1.
*/
package models

import scala.util.Try

import javax.inject.Inject
import javax.inject.Singleton
import play.api.Configuration
import utils.Converter

case class InputForm (val input: Option[String])

@Singleton
class InputFormService @Inject() (val service: DrupalImageService, val config: Configuration) {

	private val regex = config.underlying.getString("conversion.path.source").r
	private val size = Try(config.underlying.getString("image.target.size")).getOrElse("")
	private val sizeOpt = if (size.trim().isEmpty) None else Some(size)
	private val converter = new Converter(regex, sizeOpt)(service)

  def convert (form: InputForm): Option[String] = form.input match {
    case Some(string) ⇒ Some(converter.convert(string))
    case None ⇒ None
  }

}