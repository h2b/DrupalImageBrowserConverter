/*
  Drupal ImageBrowser Converter

  Copyright 2016 Hans-Hermann Bode

  Licensed under the EUPL V.1.1.
*/
package models

import anorm._
import anorm.SqlParser._
import javax.inject.Inject
import javax.inject.Singleton
import play.api.db.DBApi

/**
 * Represents a Drupal 6 "files" table described by
 * {{{
 * +-----------+------------------+------+-----+---------+----------------+
 * | Field     | Type             | Null | Key | Default | Extra          |
 * +-----------+------------------+------+-----+---------+----------------+
 * | fid       | int(10) unsigned | NO   | PRI | NULL    | auto_increment |
 * | uid       | int(10) unsigned | NO   | MUL | 0       |                |
 * | filename  | varchar(255)     | NO   |     |         |                |
 * | filepath  | varchar(255)     | NO   |     |         |                |
 * | filemime  | varchar(255)     | NO   |     |         |                |
 * | filesize  | int(10) unsigned | NO   |     | 0       |                |
 * | status    | int(11)          | NO   | MUL | 0       |                |
 * | timestamp | int(10) unsigned | NO   | MUL | 0       |                |
 * +-----------+------------------+------+-----+---------+----------------+
 * }}}
 *
 * @author h2b
 */
case class DrupalFiles (fid: Int, uid: Int,
    filename: String, filepath: String, filemime: String,
    filesize: Int, status: Int, timestamp: Int)

@Singleton
class DrupalFilesService @Inject() (dbapi: DBApi) {

  val parser: RowParser[DrupalFiles] =
    get[Int]("drupal_files.fid") ~
    get[Int]("drupal_files.uid") ~
    get[String]("drupal_files.filename") ~
    get[String]("drupal_files.filepath") ~
    get[String]("drupal_files.filemime") ~
    get[Int]("drupal_files.filesize") ~
    get[Int]("drupal_files.status") ~
    get[Int]("drupal_files.timestamp") map {
      case fid~uid~filename~filepath~filemime~filesize~status~timestamp ⇒
        DrupalFiles(fid, uid, filename, filepath, filemime, filesize, status, timestamp)
    }

  private val db = dbapi.database("default")

  private val queryAll = SQL("select * from drupal_files")

  def selectAll (): List[DrupalFiles] = db.withConnection { implicit connection ⇒
	  queryAll.as(parser.*)
  }

}
