/*
  Drupal ImageBrowser Converter

  Copyright 2016 Hans-Hermann Bode

  Licensed under the EUPL V.1.1.
*/
package controllers

import javax.inject._
import play.api._
import play.api.mvc._

/**
 * Creates an action to handle HTTP requests to the application's home page.
 */
@Singleton
class Application @Inject() extends Controller {

  def index = Action {
    Ok(views.html.index("Hello world!"))
  }

}
