/*
  Drupal ImageBrowser Converter

  Copyright 2016 Hans-Hermann Bode

  Licensed under the EUPL V.1.1.
*/
package controllers

import javax.inject._
import models.InputForm
import models.InputFormService
import play.api._
import play.api.data._
import play.api.data.Forms._
import play.api.i18n._
import play.api.mvc._

/**
 * Creates an action to handle HTTP requests to the application's input form.
 */
@Singleton
class Input @Inject() (val service: InputFormService, val messagesApi: MessagesApi) extends Controller with I18nSupport {

  val form = Form(
	  mapping(
		  "paste input" -> optional(text)
		)(InputForm.apply)(InputForm.unapply)
	)

  def show = Action {
    Ok(views.html.inputForm(form))
  }

  def convert = Action { implicit request =>
    form.bindFromRequest.fold(
      errors => BadRequest(views.html.inputForm(errors)),
      form => {
	      val textOpt = service.convert(form)
	      Ok(views.html.output(textOpt.getOrElse("")))
      }
	  )
  }

}
