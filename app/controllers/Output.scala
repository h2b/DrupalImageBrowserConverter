/*
  Drupal ImageBrowser Converter

  Copyright 2016 Hans-Hermann Bode

  Licensed under the EUPL V.1.1.
*/
package controllers

import javax.inject._
import play.api._
import play.api.mvc._

/**
 * Creates an action to show the output page.
 */
@Singleton
class Output @Inject() (val text: String) extends Controller {

  def show = Action {
    Ok(views.html.output(text))
  }

}
