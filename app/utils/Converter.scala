/*
  Drupal ImageBrowser Converter

  Copyright 2016 Hans-Hermann Bode

  Licensed under the EUPL V.1.1.
*/
package utils

import scala.util.Try
import scala.util.matching.Regex

import models.DrupalFiles
import models.DrupalImage
import models.DrupalImageService

/**
 * Converts pattern matches to file paths.
 *
 * @author h2b
 *
 * @constructor
 *
 * @param regex the regular expression that defines the pattern to be replaced
 * @param size optional size of the target image (as defined by
 * `DrupalImage.imagesize`); `None` means same size as source image
 * @param imageService the image service (implicit)
 */
class Converter (val regex: Regex, val sizeOpt: Option[String]) (implicit val imageService: DrupalImageService) {

	private val imagesWithFiles = imageService.selectAllWithFiles()
	private val imageMap = (imagesWithFiles map {
	  x: (DrupalImage, DrupalFiles) ⇒ (x._1.nid, x._1.imagesize) → x._2.filepath
	}).toMap

	/**
	 * @param text
	 * @return the original text with all matches of the regex replaced by a
	 * path to the corresponding image file; if some conversion fails, the original
	 * pattern is kept prepended by `failMarker`
	 */
	def convert (text: String): String =
    regex replaceAllIn (text, m ⇒
      Try("\"/" + imageMap(((m group 1).toInt, newSize(m group 2))) + "\"").getOrElse(Converter.failMarker+m))

	private def newSize (oldSize: String) = sizeOpt match {
	  case Some(size) ⇒ size
	  case None ⇒ oldSize
	}

}

object Converter {

  final val failMarker = "<!-- CANNOT CONVERT -->"

}
